const express = require('express');
const dotenv = require('dotenv');
const morgan = require('morgan');
const connectDB = require('./config/db');
const colors = require('colors');
const path = require('path');
const errorHandler = require('./middleware/error');
const fileUpload = require('express-fileupload');
//Load env
dotenv.config({ path: './config/config.env' });

console.log(process.env.NODE_ENV);

// Connect to DB
connectDB();

// Route files
const ads = require('./routes/ads');
const categories = require('./routes/categories');
// Create express app
const app = express();

// Body parser
app.use(express.json());

// Dev logging middleware
if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'));
}

// File upload
app.use(fileUpload());

// Set static folder
app.use(express.static(path.join(__dirname, 'public')));

// Mount routers
// Routları farklı dosyadan routes/ads.js ten alıyoruz o da controllers/ads.jsten alıyor
// Bu komutla '/api/v1/ads' endpoint ile routları bağla demiş oluyoruz
app.use('/api/v1/ads', ads);
app.use('/api/v1/categories', categories);

app.use(errorHandler);

const PORT = process.env.PORT || 5000;

const server = app.listen(
  PORT,
  console.log(
    `Server running in ${process.env.NODE_ENV} mode on port ${PORT}`.yellow.bold
  )
);
// Handle unhandled rejection (Exception)

process.on('unhandledRejection', (err, promise) => {
  console.log(`Error: ${err.message}`.red);
  // close server & exit proccess
  server.close(() => process.exit(1));
});

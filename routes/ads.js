const express = require('express');
const {
  getAds,
  getAd,
  updateAd,
  createAd,
  deleteAd,
  adsPhotoUpload,
  adsPhotoDelete
} = require('../controllers/ads');

const router = express.Router();

router.route('/:id/photo').put(adsPhotoUpload);
router.route('/:id/photo/:file_name').delete(adsPhotoDelete);

router
  // '/' tan sonrası server.js dosyasındaki app.use('/api/v1/ads', router/ads) tan geliyor
  // sonra bunlar controllerdaki metotlara bağlanıor.
  .route('/')
  .get(getAds)
  .post(createAd);

router
  .route('/:id')
  .get(getAd)
  .put(updateAd)
  .delete(deleteAd);

module.exports = router;

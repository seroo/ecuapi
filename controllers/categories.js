const ErrorResponse = require('../utils/errorResponse');
const Category = require('../models/Category');
const asyncHandler = require('../middleware/async');

/**
 * @desc  Get all categories
 * @route GET /api/v1/categories
 * @access Public
 */

exports.getCategories = asyncHandler(async (req, res, next) => {
  // console.log(req.);

  const categories = await Category.find();
  res.status(200).json({
    success: true,
    count: categories.length,
    data: categories
  });
});

/**
 * @desc  Get single category
 * @route GET /api/v1/categories/:id
 * @access Public
 */

exports.getCategory = asyncHandler(async (req, res, next) => {
  const category = await Category.findById(req.params.id);

  if (!category) {
    return next(
      new ErrorResponse(`Category not found with id of ${req.params.id}`, 404)
    );
  }
  res.status(200).json({
    success: true,
    data: category
  });
});
/**
 * @desc  Create category
 * @route POST /api/v1/categories
 * @access Private
 */
exports.createCategory = asyncHandler(async (req, res, next) => {
  const newCategory = await Category.create(req.body);
  res.status(201).json({ success: true, data: newCategory });
});
/**
 * @desc  Update an category
 * @route PUT /api/v1/categories/:id
 * @access Private
 */
exports.updateCategory = asyncHandler(async (req, res, next) => {
  const category = await Category.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true
  });
  if (!category) {
    return next(
      new ErrorResponse(`Category not found with id of ${req.params.id}`, 404)
    );
  }
  res.status(200).json({ success: true, data: category });
});

/**
 * @desc  Delete an category
 * @route DELETE /api/v1/categories/:id
 * @access Private
 */
exports.deleteCategory = asyncHandler(async (req, res, next) => {
  const category = await Category.findById(req.params.id);
  if (!category) {
    return next(
      new ErrorResponse(`Category not found with id of ${req.params.id}`, 404)
    );
  }
  const categoryDeleted = await Category.findByIdAndDelete(req.params.id);
  if (!categoryDeleted) {
    return next(
      new ErrorResponse(`Category not found with id of ${req.params.id}`, 404)
    );
  }
  res.status(200).json({ success: true, data: category });
});

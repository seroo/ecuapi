const ErrorResponse = require('../utils/errorResponse');
const path = require('path');
const fs = require('fs');
const randomString = require('randomstring');
const Ad = require('../models/Ad');
const asyncHandler = require('../middleware/async');

/**
 * @desc  Get all ads
 * @route GET /api/v1/ads
 * @access Public
 */
// Bu metotların hepsi router/ads ile bağlı url i ordan alıyor
exports.getAds = asyncHandler(async (req, res, next) => {
  // burada find() icine query i modifiye edip obje konabiliyor. query filtrelemek için yapılıyor
  // orneğin URL nin sonun ?price[lte]=1000 yazip sorgu çekilebiliyor lte= less than equal
  // o yüzden find() ı bi daha düzenlemiş olduk
  console.log(req.query);
  let query;
  // Copy req.query
  const reqQuery = { ...req.query };

  // Fields to exclude
  // bunlar varsa ayıkla, işle bunlar
  const removeFields = ['select', 'sort', 'page', 'limit'];

  // Loop over removeFields and delete them from requestQuery
  removeFields.forEach(param => delete reqQuery[param]);

  console.log(reqQuery);

  // Create query string
  let queryString = JSON.stringify(reqQuery);
  // Create operators ($gt, $gte, etc.)
  queryString = queryString.replace(
    /\b(gt|gte|lt|lte|in)\b/g,
    match => `$${match}`
  );
  // Finding resources
  // select=header,price,isArchived&price=150 şöyle sorgular yapmak için
  query = Ad.find(JSON.parse(queryString));

  // SELECT FIELDS
  // Fieldlar arası virgül olmamalı
  if (req.query.select) {
    const fields = req.query.select.split(',');
    query = query.select(fields);
  }

  // Sort '-' desc anlamında
  if (req.query.sort) {
    const sortBy = req.query.sort.split(',').join(' ');
    query = query.sort(sortBy);
  } else {
    query = query.sort('-createdAt');
  }

  // Pagination
  const page = parseInt(req.query.page, 10) || 1;
  const limit = parseInt(req.query.limit, 10) || 10;
  const startIndex = (page - 1) * limit;
  const endIndex = page * limit;
  const total = await Ad.countDocuments();
  query = query.skip(startIndex).limit(limit);

  // excuting query
  const ads = await query;
  // Pagination result
  const pagination = {};
  if (endIndex < total) {
    pagination.next = {
      page: page + 1,
      limit: limit,
      total: total
    };
  }
  if (startIndex > 0) {
    pagination.prev = {
      page: page - 1,
      limit: limit,
      total: total
    };
  }
  res.status(200).json({
    success: true,
    count: ads.length,
    pagination: pagination,
    data: ads
  });
});
/**
 * @desc  Get single ad
 * @route GET /api/v1/ads/:id
 * @access Public
 */
exports.getAd = asyncHandler(async (req, res, next) => {
  const ad = await Ad.findById(req.params.id);

  if (!ad) {
    return next(
      new ErrorResponse(`Ad not found with id of ${req.params.id}`, 404)
    );
  }

  res.status(200).json({ success: true, data: ad });
});
/**
 * @desc  Create new ad
 * @route POST /api/v1/ads
 * @access Private
 */
exports.createAd = asyncHandler(async (req, res, next) => {
  const newAd = await Ad.create(req.body);
  res.status(201).json({ success: true, data: newAd });
});
/**
 * @desc  Update an ad
 * @route PUT /api/v1/ads/:id
 * @access Private
 */
exports.updateAd = asyncHandler(async (req, res, next) => {
  const ad = await Ad.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true
  });
  if (!ad) {
    return next(
      new ErrorResponse(`Ad not found with id of ${req.params.id}`, 404)
    );
  }
  res.status(200).json({ success: true, data: ad });
});
/**
 * @desc  Delete an ad
 * @route DELETE /api/v1/ads/:id
 * @access Private
 */
exports.deleteAd = asyncHandler(async (req, res, next) => {
  const ad = await Ad.findById(req.params.id);
  if (!ad) {
    return next(
      new ErrorResponse(`Ad not found with id of ${req.params.id}`, 404)
    );
  }
  const adDeleted = await Ad.findByIdAndDelete(req.params.id);
  if (!adDeleted) {
    return next(
      new ErrorResponse(`Ad not found with id of ${req.params.id}`, 404)
    );
  }
  res.status(200).json({ success: true, data: ad });
});
/**
 * @desc  Upload a photo of ads
 * @route PUT /api/v1/ads/:id/photo
 * @access Private
 */
exports.adsPhotoUpload = asyncHandler(async (req, res, next) => {
  const ad = await Ad.findById(req.params.id);
  if (!ad) {
    return next(
      new ErrorResponse(`Ad not found with id of ${req.params.id}`, 404)
    );
  }

  if (!req.files) {
    return next(new ErrorResponse(`Please upload a file`, 404));
  }
  const file = req.files.file;
  // check is image
  if (!file.mimetype.startsWith('image')) {
    return next(new ErrorResponse(`Please upload an image file`, 404));
  }

  if (file.size > process.env.MAX_FILE_UPLOAD_SIZE) {
    return next(
      new ErrorResponse(
        `Please upload an image less than ${process.env.MAX_FILE_UPLOAD_SIZE /
          1000}KB `,
        404
      )
    );
  }

  // Create custom file name and extension (uzantısını oath modulu ile yapıyoruz)
  file.name = `photo_${randomString.generate(24)}${path.parse(file.name).ext}`;

  file.mv(`${process.env.FILE_UPLOAD_PATH}/${file.name}`, async err => {
    if (err) {
      console.log(err);
      return next(new ErrorResponse(`Problem with file upload`, 500));
    }

    await Ad.findByIdAndUpdate(req.params.id, { $push: { photos: file.name } });

    res.status(200).json({
      success: true,
      data: file.name
    });
  });

  console.log(file.name);
});
//------------------

/**
 * @desc  Delete a photo of ad
 * @route DELETE /api/v1/ads/:id/photo/:file_name
 * @access Private
 */
exports.adsPhotoDelete = asyncHandler(async (req, res, next) => {
  const ad = await Ad.findById(req.params.id);
  const fileName = req.params.file_name;

  if (!ad) {
    return next(
      new ErrorResponse(`Ad not found with id of ${req.params.id}`, 404)
    );
  }

  await Ad.updateOne(
    { _id: req.params.id },
    { $pull: { photos: fileName } },
    err => {
      if (err) {
        console.log(err);
        return next(
          new ErrorResponse(`Photo could not deleted ${fileName}`, 404)
        );
      }
    }
  );

  await fs.unlink(`${process.env.FILE_UPLOAD_PATH}/${fileName}`, err => {
    if (err) {
      console.log(err);
      return next(
        new ErrorResponse(`Photo could not deleted ${fileName}`, 404)
      );
    }
  });

  console.log(`Photo ${fileName} is deleted`);

  res.status(200).json({
    success: true,
    data: fileName
  });
});

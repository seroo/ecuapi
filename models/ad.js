const mongoose = require('mongoose');
const slugify = require('slugify');
const AdSchema = new mongoose.Schema(
  {
    header: {
      type: String,
      required: [true, 'Please add a header'],
      // FIXME bunu sil sonra
      unique: true,
      trim: true,
      maxlength: [50, 'Header can not be more than 50 characters']
    },
    slug: String,
    price: {
      type: Number,
      required: [true, 'Please add a price']
    },
    description: {
      type: String,
      required: [true, 'Please add a description'],
      maxlength: [500, 'Description can not be more than 500 characters']
    },
    photos: {
      type: [String],
      default: 'no-photo.jpg'
    },
    isArchived: {
      type: Boolean,
      default: false
    },
    isSold: {
      type: Boolean,
      default: false
    },
    category: {
      type: mongoose.Schema.ObjectId,
      ref: 'Ad',
      required: 'true'
    }
  },
  { timestamps: true }
);
//create ad slug from the header
// schemanın scope unda kalması için arrow func kullanmadık
AdSchema.pre('save', function() {
  // slug field ına name i al kucukharfle yaz
  this.slug = slugify(this.header, { lower: true });
  next();
});

module.exports = mongoose.model('Ad', AdSchema);

const mongoose = require('mongoose');

const CategorySchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Please add a category name'],
    unique: true
  }
});

module.exports = mongoose.model('Category', CategorySchema);

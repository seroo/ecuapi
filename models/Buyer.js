const mongoose = require('mongoose');

const BuyerSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, 'Please add a category name'],
      unique: false
    },
    adress: {
      type: String,
      maxlength: 200
    },
    tel: {
      type: String,
      maxlength: 13
    },
    email: {
      type: String,
      match: [
        /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
        'Please add a valid email'
      ]
    },
    quickReminders: {
      type: String,
      maxlength: 100
    },
    soldItems: {
      type: [String]
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model('Buyer', BuyerSchema);
